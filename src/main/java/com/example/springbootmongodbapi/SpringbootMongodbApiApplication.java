package com.example.springbootmongodbapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMongodbApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMongodbApiApplication.class, args);
    }

}
