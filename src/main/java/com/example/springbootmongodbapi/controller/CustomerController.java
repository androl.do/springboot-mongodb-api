package com.example.springbootmongodbapi.controller;

import com.example.springbootmongodbapi.exception.CustomerException;
import com.example.springbootmongodbapi.model.Customer;
import com.example.springbootmongodbapi.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    /*Get all customer*/
    @GetMapping()
    public ResponseEntity<?> getAllCustomers(){
        List<Customer> customers = customerService.getAllCustomers();
        return new ResponseEntity<>(customers,customers.size()>0 ? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }

    /*Create new customer*/
    @PostMapping
    public ResponseEntity<?> createCustomer(@RequestBody Customer customer){
        try {
            return new ResponseEntity<>(customerService.createCustomer(customer),HttpStatus.OK);
        }catch (CustomerException e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
        }
    }

    /*Update customer*/
    @PutMapping("/{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable("id") String id, @RequestBody Customer customer){
        try {
            return new ResponseEntity<>(customerService.updateCustomer(id,customer),HttpStatus.OK);
        }catch (CustomerException e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
        }
    }

    /*Delete data customer*/
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable("id") String id){
        try {
            customerService.deleteCustomer(id);
            return new ResponseEntity<>("Delete customer with id: " +id+ " successfully", HttpStatus.OK);
        }catch (CustomerException e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
        }
    }

}
