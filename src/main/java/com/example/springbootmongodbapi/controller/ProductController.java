package com.example.springbootmongodbapi.controller;

import com.example.springbootmongodbapi.exception.CustomerException;
import com.example.springbootmongodbapi.exception.ProductException;
import com.example.springbootmongodbapi.model.Customer;
import com.example.springbootmongodbapi.model.Product;
import com.example.springbootmongodbapi.service.CustomerService;
import com.example.springbootmongodbapi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {
    @Autowired
    private ProductService productService;

    /*Get all customer*/
    @GetMapping()
    public ResponseEntity<?> getAllProducts(){
        List<Product> products = productService.getAllProducts();
        return new ResponseEntity<>(products,products.size()>0 ? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }

    /*Create new customer*/
    @PostMapping
    public ResponseEntity<?> createProduct(@RequestBody Product product){
        try {
            return new ResponseEntity<>(productService.createProduct(product),HttpStatus.OK);
        }catch (ProductException e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
        }
    }

    /*Update customer*/
    @PutMapping("/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable("id") String id, @RequestBody Product product){
        try {
            return new ResponseEntity<>(productService.updateProduct(id,product),HttpStatus.OK);
        }catch (ProductException e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
        }
    }

    /*Delete data customer*/
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") String id){
        try {
            productService.deleteProduct(id);
            return new ResponseEntity<>("Delete product with id: " +id+ " successfully", HttpStatus.OK);
        }catch (ProductException e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
        }
    }

}
