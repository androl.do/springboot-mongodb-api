package com.example.springbootmongodbapi.controller;

import com.example.springbootmongodbapi.dto.PasswordDto;
import com.example.springbootmongodbapi.exception.UserException;
import com.example.springbootmongodbapi.model.User;
import com.example.springbootmongodbapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.*;
import net.bytebuddy.utility.*;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private JavaMailSender mailSender;

    /*Get users , filter and sort*/
    @GetMapping()
    public ResponseEntity<?> getUsersByFilter(@RequestParam(value = "firstName",required = false) String firstName,
                                         @RequestParam(value = "lastName",required = false) String lastName,
                                         @RequestParam(value = "status",required = false) Boolean status,
                                         @RequestParam(value = "sort",required = false) String sort){
        try{
            List<User> users = userService.getUsersByFilterAndSort(firstName, lastName, status, sort);
            return new ResponseEntity<>(users,users.size()>0 ? HttpStatus.OK:HttpStatus.NOT_FOUND);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    /*Create new user*/
    @PostMapping()
    public ResponseEntity<?> createUser(@RequestBody User user){
        try {
            System.err.println(user.getUsername());
            user =  userService.createUser(user);
            return new ResponseEntity<>(user, HttpStatus.OK);
        }catch (UserException e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.CONFLICT);
        }
    }

    /*Update infor user*/
    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@PathVariable("id") String id,@RequestBody User user){
        try {
            user =  userService.updateUser(id,user);
            return new ResponseEntity<>(user, HttpStatus.OK);
        }catch (UserException e){
            return new ResponseEntity<>(e.getMessage(),e.equals(UserException.notFoundException(id)) ? HttpStatus.NOT_FOUND:HttpStatus.CONFLICT);
        }
    }

    /*Delete user not remove data, change status to false*/
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") String id){
        try {
            userService.deleteUser(id);
            return new ResponseEntity<>("Delete user with id: " + id + " successfully",HttpStatus.OK);
        }catch (UserException e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
        }
    }

    /*Search user by text search first,last name*/
    @GetMapping("/search")
    public ResponseEntity<?> searchUserByName(@RequestParam("key") String key){
        List<User> users = userService.searchUserByName(key);
        return new ResponseEntity<>(users,users.size()>0 ? HttpStatus.OK:HttpStatus.NOT_FOUND);
    }

    /*Change password user*/
    @PutMapping("/change-password/{id}")
    public ResponseEntity<?> changePassword(@PathVariable("id") String id, @RequestBody PasswordDto passwordDto){
        try {
            userService.changePassword(id,passwordDto);
            return new ResponseEntity<>("Change password successfully",HttpStatus.OK);
        }catch (UserException e){
            return new ResponseEntity<>(e.getMessage(),e.equals(UserException.notFoundException(id)) ? HttpStatus.NOT_FOUND:HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    /*Forget password - send token to email and save token*/
    @GetMapping("/forget-password")
    public ResponseEntity<?> forgetPassword(@RequestParam("email") String email){
        Optional<User> userOpt = userService.getUserByEmail(email);
        if (userOpt.isPresent()){
            String token = RandomString.make(30);
            try {
                sendEmail(email,token);
                User user = userOpt.get();
                user.setTokenResetPassword(token);
                userService.updateUser(user);
                return new ResponseEntity<>("Token has been sent to the mail",HttpStatus.OK);
            } catch (MessagingException | UnsupportedEncodingException e){
                return new ResponseEntity<>("Send mail error",HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }else{
            return new ResponseEntity<>("Not fount email: " + email,HttpStatus.NOT_FOUND);
        }
    }

    /*Reset password - check token, if true update new password for user*/
    @PutMapping("/reset-password")
    public ResponseEntity<?> resetPassword(@RequestParam("email") String email, @RequestBody PasswordDto passwordDto){
        Optional<User> userOpt = userService.getUserByEmail(email);
        if (userOpt.isPresent()){
            User user = userOpt.get();
            if (passwordDto.getToken().length()>0 && user.getTokenResetPassword().equalsIgnoreCase(passwordDto.getToken())){
                userService.resetPassword(user);
                return new ResponseEntity<>("Reset password successfully",HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>("Token incorrect",HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }else{
            return new ResponseEntity<>("Not fount email: " + email,HttpStatus.NOT_FOUND);
        }
    }

    /*Send token to email of user*/
    public void sendEmail(String recipientEmail, String token)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("contact@trungngoc.com", "TrungNgoc Support");
        helper.setTo(recipientEmail);

        String subject = "Here's the token to reset your password";

        String content = "Token: " + token;

        helper.setSubject(subject);

        helper.setText(content, true);

        mailSender.send(message);
    }

}
