package com.example.springbootmongodbapi.exception;

public class ProductException extends  Exception{

    public ProductException(String message) {
        super(message);
    }

    public  static String notFoundException(String id){
        return "Product with id: " + id + " not found";
    }

    public  static String productCodeAlreadyExist(){
        return "This product code already exist";
    }

}
