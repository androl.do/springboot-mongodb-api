package com.example.springbootmongodbapi.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("customers")
public class Customer {
    @Id
    private String id;
    private String name;
    @Indexed(unique = true)
    private String phone;
    @Indexed(unique = true)
    private String email;
    private Address address;
}
