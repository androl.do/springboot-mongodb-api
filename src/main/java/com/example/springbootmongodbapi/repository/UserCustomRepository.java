package com.example.springbootmongodbapi.repository;

import com.example.springbootmongodbapi.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface UserCustomRepository {
    List<User> findUsersByProperties(String firstName, String lastName, Boolean status, String sort) throws Exception;
}
