package com.example.springbootmongodbapi.repository.repositoryImpl;

import com.example.springbootmongodbapi.model.User;
import com.example.springbootmongodbapi.repository.UserCustomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserCustomRepositoryImpl implements UserCustomRepository {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<User> findUsersByProperties(String firstName, String lastName, Boolean status, String sort) throws Exception {
        Query query = new Query();
        List<Criteria> criteria = new ArrayList<>();

        if (firstName!=null && !firstName.isEmpty()){
            criteria.add(Criteria.where("firstName").is(firstName));
        }
        if (lastName!=null && !lastName.isEmpty()){
            criteria.add(Criteria.where("lastName").is(lastName));
        }
        if (status!=null){
            criteria.add(Criteria.where("status").is(status));
        }

        if (!criteria.isEmpty()){
            query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
        }


        if (sort!=null && !sort.isEmpty()){
            String[] sortArr = sort.split(":");
            if (sortArr.length==2){
               try {
                   int sortDir = Integer.parseInt(sortArr[1]);
                   query.with(Sort.by(sortDir==1? Sort.Direction.ASC: Sort.Direction.DESC,sortArr[0]));
               }catch (Exception e){
                   throw new Exception("Filter param incorrect",e);
               }
            }
        }

        return mongoTemplate.find(query,User.class);
    }
}
