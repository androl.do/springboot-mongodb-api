package com.example.springbootmongodbapi.service;

import com.example.springbootmongodbapi.exception.CustomerException;
import com.example.springbootmongodbapi.model.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomers();

    Customer createCustomer(Customer newCus) throws CustomerException;

    Customer updateCustomer(String id, Customer cus) throws CustomerException;

    void deleteCustomer(String id) throws CustomerException;
}
