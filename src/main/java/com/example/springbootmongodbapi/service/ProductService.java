package com.example.springbootmongodbapi.service;

import com.example.springbootmongodbapi.exception.ProductException;
import com.example.springbootmongodbapi.model.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAllProducts();

    Product createProduct(Product newProduct) throws ProductException;

    Product updateProduct(String id, Product newProduct) throws ProductException;

    void deleteProduct(String id) throws  ProductException;
}
