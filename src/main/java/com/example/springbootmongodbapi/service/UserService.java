package com.example.springbootmongodbapi.service;

import com.example.springbootmongodbapi.dto.PasswordDto;
import com.example.springbootmongodbapi.exception.UserException;
import com.example.springbootmongodbapi.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    User createUser(User newUser) throws UserException;

    List<User> getAllUsers();

    User updateUser(String id, User user) throws UserException;

    void deleteUser(String id) throws UserException;

    List<User> searchUserByName(String key);

    void changePassword(String id, PasswordDto passwordDto) throws UserException;

    Optional<User> getUserByEmail(String email);

    void updateUser(User user);

    void resetPassword(User user);

    List<User> getUsersByFilterAndSort(String firstName, String lastName, Boolean status, String sort) throws Exception;
}
