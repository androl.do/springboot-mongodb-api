package com.example.springbootmongodbapi.service.serviceImpl;

import com.example.springbootmongodbapi.exception.ProductException;
import com.example.springbootmongodbapi.model.Product;
import com.example.springbootmongodbapi.repository.ProductRepository;
import com.example.springbootmongodbapi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepo;

    @Override
    public List<Product> getAllProducts(){
        List<Product>  products = productRepo.findAll();
        if (!products.isEmpty()){
            return products;
        }else{
            return new ArrayList<>();
        }
    }

    @Override
    public Product createProduct(Product newProduct) throws ProductException {
        if (productRepo.findProductByCode(newProduct.getCode()).isPresent()){
            throw  new ProductException(ProductException.productCodeAlreadyExist());
        }else{
            return productRepo.save(newProduct);
        }
    }

    @Override
    public Product updateProduct(String id, Product newProduct) throws ProductException {
        Optional<Product> productOpt = productRepo.findById(id);
        if (!productOpt.isPresent()){
            throw new ProductException(ProductException.notFoundException(id));
        }else{
            Product oldProduct = productOpt.get();
            oldProduct.setName(newProduct.getName()==null ? oldProduct.getName() : newProduct.getName());
            oldProduct.setPrice(newProduct.getPrice()==null ? oldProduct.getPrice() : newProduct.getPrice());
            return productRepo.save(oldProduct);
        }
    }

    @Override
    public void deleteProduct(String id) throws  ProductException{
        Optional<Product> productOpt = productRepo.findById(id);
        if (!productOpt.isPresent()){
            throw new ProductException(ProductException.notFoundException(id));
        }else{
            productRepo.delete(productOpt.get());
        }
    }


}
