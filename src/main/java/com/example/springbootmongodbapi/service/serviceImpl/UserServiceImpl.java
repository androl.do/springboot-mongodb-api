package com.example.springbootmongodbapi.service.serviceImpl;

import com.example.springbootmongodbapi.dto.PasswordDto;
import com.example.springbootmongodbapi.exception.UserException;
import com.example.springbootmongodbapi.model.User;
import com.example.springbootmongodbapi.repository.UserCustomRepository;
import com.example.springbootmongodbapi.repository.UserRepository;
import com.example.springbootmongodbapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEn;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private UserCustomRepository userCustomRepo;

    @Override
    public User createUser(User newUser) throws UserException {
        if (userRepo.findUserByUsername(newUser.getUsername()).isPresent()) {
            throw new UserException(UserException.usernameAlreadyExist());
        }
        if (userRepo.findUserByEmail(newUser.getEmail()).isPresent()) {
            throw new UserException(UserException.emailAlreadyExist());
        }
        if (userRepo.findUserByPhone(newUser.getPhone()).isPresent()) {
            throw new UserException(UserException.phoneAlreadyExist());
        }

        newUser.setPassword(bCryptPasswordEn.encode(newUser.getPassword()));
        newUser.setStatus(true);
        return userRepo.save(newUser);

    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = userRepo.findAll();
        if (!users.isEmpty()) {
            return users;
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public User updateUser(String id, User user) throws UserException {
        Optional<User> useOp = userRepo.findById(id);

        if (!useOp.isPresent()) {
            throw new UserException(UserException.notFoundException(id));
        }

        User oldUser = useOp.get();
        if (user.getUsername() != null) {
            if (userRepo.findUserByUsername(user.getUsername()).isPresent()) {
                throw new UserException(UserException.usernameAlreadyExist());
            } else {
                oldUser.setUsername(user.getUsername());
            }
        }
        if (user.getEmail() != null) {
            if (userRepo.findUserByEmail(user.getEmail()).isPresent()) {
                throw new UserException(UserException.emailAlreadyExist());
            } else {
                oldUser.setEmail(user.getEmail());
            }
        }
        if (user.getPhone() != null) {
            if (userRepo.findUserByPhone(user.getPhone()).isPresent()) {
                throw new UserException(UserException.phoneAlreadyExist());
            } else {
                oldUser.setPhone(user.getPhone());
            }
        }
        oldUser.setFirstName(user.getFirstName() == null ? oldUser.getFirstName() : user.getFirstName());
        oldUser.setLastName(user.getLastName() == null ? oldUser.getLastName() : user.getLastName());
        oldUser.setStatus(user.getStatus() == null ? oldUser.getStatus() : user.getStatus());
        oldUser.setAddress(user.getAddress() == null ? oldUser.getAddress() : user.getAddress());
        return userRepo.save(oldUser);

    }

    @Override
    public void deleteUser(String id) throws UserException {
        Optional<User> userOp = userRepo.findById(id);
        if (userOp.isPresent()) {
            User oldUser = userOp.get();
            oldUser.setStatus(false);
            userRepo.save(oldUser);
        } else {
            throw new UserException(UserException.notFoundException(id));
        }
    }

    /* search user by key in first name or last name*/
    @Override
    public List<User> searchUserByName(String key) {
        List<User> users = userRepo.searchByName(key);
        if (!users.isEmpty()) {
            return users;
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public void changePassword(String id, PasswordDto passwordDto) throws UserException {
        Optional<User> userOp = userRepo.findById(id);
        if (userOp.isPresent()) {
            User oldUser = userOp.get();
            if (bCryptPasswordEn.matches(passwordDto.getOldPassword(), oldUser.getPassword())) {
                oldUser.setPassword(bCryptPasswordEn.encode(passwordDto.getNewPassword()));
            } else {
                throw new UserException("The old password incorrect");
            }
        } else {
            throw new UserException(UserException.notFoundException(id));
        }
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        return userRepo.findUserByEmail(email);
    }

    @Override
    public void updateUser(User user) {
        userRepo.save(user);
    }

    @Override
    public void resetPassword(User user) {
        user.setPassword(bCryptPasswordEn.encode(user.getPassword()));
        user.setTokenResetPassword("");
        userRepo.save(user);
    }

    @Override
    public List<User> getUsersByFilterAndSort(String firstName, String lastName, Boolean status, String sort) throws Exception {
        List<User> users = userCustomRepo.findUsersByProperties(firstName, lastName, status, sort);
        if (!users.isEmpty()) {
            return users;
        } else {
            return new ArrayList<>();
        }
    }


}
